# Software Under The Desktop Broonsepar
This project is intended to provide an interface between clients with software projects and freelancers
Remotely, so customers can do their own projects
## Features of this system
* Providing a platform for communication between the people who have the project and the freelancers they specialize in
* The power of customer selection among different programmers

## Analysis and design of the project

In this section, we will analyze and design the project

*  [Activity Diagram](documentation/Activity.md)

*  [UseCase Diagram](documentation/UseCase Diagram.md)

*  [Sequence Diagram](documentation/Sequence.md)

*  [Class Diagram](documentation/ClassDiagram.md)

*  [Database Diagram](documentation/Database.md)

*  [Requirment](documentation/REQUIRMENT.md)

*  [Scenario-customer](documentation/SCENARIO_customer.md)

*  [Scenario-freelancer](documentation/SCENARIO_freelancer.md)



## Project development phases
1. Website design
    * sign in to site
    * Register
    * Software rules
2. Registration page for freelancers
3. Customer registration page
4. List of projects not implemented
6. Selected Projects Page
7. Private page of freelancers
8. Customer home page
9.  Freelancer selection page
10. System database design

## Developers

| ID |first name and last name|
| :---- | ----: |
| @SorooshMortazavi |Soorosh Mortazavi | 
| @mehdi_t1995 |Mehdi Tavangar | 
