## The functional requirements of the Broonsepar system

1. User membership feature
2. Ability to record freelance skills
3. The ability to record resume frilensar
4. Project creation capability
5. The ability to record a proposal for a project
6. Ability to talk about the project
7. Possibility to categorize topics
8. Search feature by keywords
9. Ability to rate work for freelancers
10. Visibility of ratings of freelancers based on new criteria (functional)
11. Payment for membership
12. See suggestions for the project Freelancer by contractor
13. Complete the project features by the employer
14. Lowering the money from the employer's account after creating the project

