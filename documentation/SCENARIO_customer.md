## Customer scenario

1. The client logs on to its user panel in the software
2. The project records its required software in the database and can be priced and time-consuming
3. The customer project, which has been seen by freelancers, receives suggestions along with the price and timing
4. The customer chooses the best offer and his project is done
