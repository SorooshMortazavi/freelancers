## Freelancer's Scenario
 
1. Sign in to your account
2. Checking projects that have not been done
3. They propose a project to one of the projects
4. Waiting for acceptance of the request from the client side
5. If customers accept the project they will make the project and receive the money from the customer
6. If they do not accept the offer, they will go to another project from the client side
